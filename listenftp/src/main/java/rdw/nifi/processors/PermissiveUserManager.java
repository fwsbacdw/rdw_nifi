package rdw.nifi.processors;

import org.apache.ftpserver.ftplet.Authentication;
import org.apache.ftpserver.ftplet.AuthenticationFailedException;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.User;
import org.apache.ftpserver.ftplet.UserManager;
import org.apache.ftpserver.usermanager.AnonymousAuthentication;
import org.apache.ftpserver.usermanager.UsernamePasswordAuthentication;
import org.apache.ftpserver.usermanager.impl.BaseUser;
import org.apache.ftpserver.usermanager.impl.ConcurrentLoginPermission;
import org.apache.ftpserver.usermanager.impl.WritePermission;

import java.util.Arrays;

/**
 * A user manager that allows anybody in.
 */
public class PermissiveUserManager implements UserManager {

    @Override
    public User getUserByName(String username) throws FtpException {
        return createUser(username);
    }

    @Override
    public String[] getAllUserNames() throws FtpException {
        return new String[0];
    }

    @Override
    public void delete(String username) throws FtpException {
        // no-op
    }

    @Override
    public void save(User user) throws FtpException {
        // no-op
    }

    @Override
    public boolean doesExist(String username) throws FtpException {
        // all users exist!
        return true;
    }

    @Override
    public User authenticate(Authentication authentication) throws AuthenticationFailedException {
        if (authentication instanceof UsernamePasswordAuthentication) {
            return createUser(((UsernamePasswordAuthentication)authentication).getUsername());
        } else if (authentication instanceof AnonymousAuthentication) {
            return createUser("anonymous");
        }
        return null;
    }

    @Override
    public String getAdminName() throws FtpException {
        return null;
    }

    @Override
    public boolean isAdmin(String username) throws FtpException {
        return false;
    }

    private User createUser(String username) {
        final BaseUser user = new BaseUser();
        user.setAuthorities(Arrays.asList(new ConcurrentLoginPermission(1, 1), new WritePermission()));
        user.setEnabled(true);
        user.setHomeDirectory(System.getProperty("java.io.tmpdir"));
        user.setName(username);
        return user;
    }
}
