package rdw.nifi.processors;

import com.google.common.net.MediaType;
import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.command.CommandFactoryFactory;
import org.apache.ftpserver.command.impl.ABOR;
import org.apache.ftpserver.command.impl.ACCT;
import org.apache.ftpserver.command.impl.APPE;
import org.apache.ftpserver.command.impl.AUTH;
import org.apache.ftpserver.command.impl.EPRT;
import org.apache.ftpserver.command.impl.EPSV;
import org.apache.ftpserver.command.impl.FEAT;
import org.apache.ftpserver.command.impl.MODE;
import org.apache.ftpserver.command.impl.NOOP;
import org.apache.ftpserver.command.impl.PASS;
import org.apache.ftpserver.command.impl.PASV;
import org.apache.ftpserver.command.impl.PBSZ;
import org.apache.ftpserver.command.impl.PROT;
import org.apache.ftpserver.command.impl.QUIT;
import org.apache.ftpserver.command.impl.REIN;
import org.apache.ftpserver.command.impl.REST;
import org.apache.ftpserver.command.impl.SIZE;
import org.apache.ftpserver.command.impl.STAT;
import org.apache.ftpserver.command.impl.STOR;
import org.apache.ftpserver.command.impl.STOU;
import org.apache.ftpserver.command.impl.STRU;
import org.apache.ftpserver.command.impl.SYST;
import org.apache.ftpserver.command.impl.TYPE;
import org.apache.ftpserver.command.impl.USER;
import org.apache.ftpserver.ftplet.DefaultFtplet;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.FtpFile;
import org.apache.ftpserver.ftplet.FtpRequest;
import org.apache.ftpserver.ftplet.FtpSession;
import org.apache.ftpserver.ftplet.Ftplet;
import org.apache.ftpserver.ftplet.FtpletResult;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.nifi.annotation.behavior.InputRequirement;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.lifecycle.OnStopped;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.flowfile.attributes.CoreAttributes;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.FlowFileAccessException;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.util.StandardValidators;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;


@InputRequirement(InputRequirement.Requirement.INPUT_FORBIDDEN)
@Tags({"rdw", "ingest", "ftp", "sftp", "listen"})
@CapabilityDescription("Starts an FTP Server that is used to receive files from remote sources.")
public class ListenFtp extends AbstractProcessor {

    public static final Relationship REL_SUCCESS = new Relationship.Builder()
            .name("success")
            .description("All FlowFiles that are received are routed to success")
            .build();

    public static final PropertyDescriptor PORT = new PropertyDescriptor
            .Builder().name("Port")
            .description("The port to listen on for communication.")
            .required(true)
            .addValidator(StandardValidators.PORT_VALIDATOR)
            .defaultValue("2121")
            .build();
    public static final PropertyDescriptor BATCH_SIZE = new PropertyDescriptor.Builder()
            .name("Batch Size")
            .description("The maximum number of files to transfer in each iteration")
            .required(true)
            .addValidator(StandardValidators.POSITIVE_INTEGER_VALIDATOR)
            .defaultValue("1")
            .build();

    private Set<Relationship> relationships;
    private List<PropertyDescriptor> descriptors;

    private FtpServer ftpServer;
    private Queue<FtpFileInfo> fileQueue = new ConcurrentLinkedQueue<>();

    @Override
    protected void init(ProcessorInitializationContext context) {
        super.init(context);

        final List<PropertyDescriptor> properties = new ArrayList<>();
        properties.add(PORT);
        properties.add(BATCH_SIZE);
        this.descriptors = Collections.unmodifiableList(properties);

        final Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_SUCCESS);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    protected List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }

    @Override
    public Set<Relationship> getRelationships() {
        return relationships;
    }

    @OnScheduled
    public void startFtpServer(final ProcessContext context) {
        fileQueue.clear();

        ftpServer = createFtpServerFactory(context).createServer();

        try {
            ftpServer.start();
        } catch (final FtpException e) {
            getLogger().error("Failed to start FTP server", e);
            ftpServer = null;
            throw new RuntimeException(e);
        }

        // TODO - do i need to create a thread and run the ftp server in it?
//        final Thread serverThread = new Thread();
//        serverThread.setName(getClass().getName() + " [" + getIdentifier() + "]");
//        serverThread.setDaemon(true);
//        serverThread.start();
    }

    @OnStopped
    public void stopFtpServer() {
        fileQueue.clear();

        if (ftpServer == null) return;

        ftpServer.stop();
        ftpServer = null;
    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        final List<FtpFileInfo> files = getAvailableFiles(context.getProperty(BATCH_SIZE).asInteger());

        if (files.isEmpty()) {
            // nothing to do, tell framework to not schedule this processor for a while
            context.yield();
            return;
        }

        try {
            for (final FtpFileInfo fileInfo : files) {
                FlowFile flowFile = session.create();
                // TODO - ? add property for keepSourceFile ?
                flowFile = session.importFrom(fileInfo.getFile().toPath(), false, flowFile);
                flowFile = session.putAttribute(flowFile, CoreAttributes.MIME_TYPE.key(), MediaType.APPLICATION_BINARY.toString());
                flowFile = session.putAttribute(flowFile, CoreAttributes.FILENAME.key(), fileInfo.getFile().getName());
                // TODO - add constants for these keys
                flowFile = session.putAttribute(flowFile, "rdw.ftp.username", fileInfo.getUserName());
                flowFile = session.putAttribute(flowFile, "rdw.ftp.source", fileInfo.getSourceIP());

                // TODO - does transitUri have to be real?
                session.getProvenanceReporter().receive(flowFile, "ftp://host:port/" + fileInfo.getFile().getAbsoluteFile());
                session.transfer(flowFile, REL_SUCCESS);
                getLogger().info("Successfully retrieved FTP file {} from {}@{}",
                        new Object[]{fileInfo.getFile().getAbsoluteFile(), fileInfo.getUserName(), fileInfo.getSourceIP()});
            }
            session.commit();
        } catch (final FlowFileAccessException e) {
            context.yield();
            getLogger().error("Error retrieving FTP file due to {}", e);
            session.rollback();
        }
    }

    private List<FtpFileInfo> getAvailableFiles(final int limit) {
        final List<FtpFileInfo> files = new ArrayList<>(limit);
        while (!fileQueue.isEmpty() && files.size() < limit) {
            final FtpFileInfo fileInfo = fileQueue.poll();
            if (fileInfo == null) break;
            files.add(fileInfo);
        }
        return files;
    }


    private FtpServerFactory createFtpServerFactory(final ProcessContext context) {
        final FtpServerFactory serverFactory = new FtpServerFactory();
        serverFactory.setUserManager(new PermissiveUserManager());

        // configure listener
        // TODO - SSL
        final ListenerFactory listenerFactory = new ListenerFactory();
        listenerFactory.setPort(context.getProperty(PORT).asInteger());
        listenerFactory.setImplicitSsl(false);
        serverFactory.addListener("default", listenerFactory.createListener());

        // configure commands; this disables some commands, especially navigation
        final CommandFactoryFactory cfFactory = new CommandFactoryFactory();
        cfFactory.setUseDefaultCommands(false);
        cfFactory.addCommand("ABOR", new ABOR());
        cfFactory.addCommand("ACCT", new ACCT());
        cfFactory.addCommand("APPE", new APPE());
        cfFactory.addCommand("AUTH", new AUTH());
        cfFactory.addCommand("EPRT", new EPRT());
        cfFactory.addCommand("EPSV", new EPSV());
        cfFactory.addCommand("MODE", new MODE());
        cfFactory.addCommand("NOOP", new NOOP());
        cfFactory.addCommand("PASS", new PASS());
        cfFactory.addCommand("PASV", new PASV());
        cfFactory.addCommand("QUIT", new QUIT());
        cfFactory.addCommand("REIN", new REIN());
        cfFactory.addCommand("REST", new REST());
        cfFactory.addCommand("STAT", new STAT());
        cfFactory.addCommand("SIZE", new SIZE());
        cfFactory.addCommand("STOR", new STOR());
        cfFactory.addCommand("STOU", new STOU());
        cfFactory.addCommand("STRU", new STRU());
        cfFactory.addCommand("SYST", new SYST());
        cfFactory.addCommand("TYPE", new TYPE());
        cfFactory.addCommand("USER", new USER());

        cfFactory.addCommand("FEAT", new FEAT());
        cfFactory.addCommand("PBSZ", new PBSZ());
        cfFactory.addCommand("PROT", new PROT());
        serverFactory.setCommandFactory(cfFactory.createCommandFactory());

        // configure event hooks
        // NOTE: this map must be mutable to avoid shutdown exception
        final Map<String, Ftplet> ftplets = new HashMap<>();
        ftplets.put("FileCatcher", new FileCatcher());
        serverFactory.setFtplets(ftplets);

        return serverFactory;
    }

    /**
     * Helper class that responds to ftp activity, specifically file upload<br/>
     */
    private class FileCatcher extends DefaultFtplet {

        @Override
        public FtpletResult onUploadEnd(FtpSession session, FtpRequest request) throws FtpException, IOException {
            fileQueue.add(new FtpFileInfo(
                    new File(workingDir(session), request.getArgument()),
                    session.getUser().getName(),
                    session.getClientAddress().getAddress().getHostAddress()
            ));
            return super.onUploadEnd(session, request);
        }

        /**
         * Helper to build the full working directory for the ftp session.
         *
         * @param session ftp session
         * @return working directory
         * @throws FtpException if problem getting file system working directory
         */
        private File workingDir(FtpSession session) throws FtpException {
            FtpFile workingDirectory = session.getFileSystemView().getWorkingDirectory();
            String home = session.getUser().getHomeDirectory();
            String absolutePath = workingDirectory.getAbsolutePath().substring(1);
            return new File(new File(home), absolutePath);
        }
    }

    private static class FtpFileInfo {
        private File file;
        private String userName;
        private String sourceIP;

        public FtpFileInfo(File file, String userName, String sourceIP) {
            this.file = file;
            this.userName = userName;
            this.sourceIP = sourceIP;
        }

        public File getFile() {
            return file;
        }

        public String getUserName() {
            return userName;
        }

        public String getSourceIP() {
            return sourceIP;
        }
    }

}
