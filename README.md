### Apache NiFi PoC
The goal of this PoC is to implement the RDW ingest pipeline using Apache NiFi.

#### Setup
You'll need to install NiFi locally. See the getting started link; for MacOS:
```bash
brew install nifi
nifi run
```

Then point a browser at http://localhost:8080/nifi

To monitor the logs, using the defaults, do something like:
```bash
cd /usr/local/Cellar/nifi/1.1.1/libexec
tail -100f logs/nifi-app.log
```

#### Build and Deploy
Once you have confirmed things are working, build the project, copy the artifact and restart nifi:
```bash
mvn clean install
cp nar/target/nifi-rdw-nar-1.0-SNAPSHOT.nar /usr/local/Cellar/nifi/1.1.1/libexec/lib/
# CTRL-C to stop
nifi run
```

This should make the `ListenFTP`, `DecryptContent` and `Udl` processors available in the UI.

#### Configure
You'll need a flow. Currently, the entire flow is contained in a single file `conf/flow.xml.gz`. A sample is included
in the project. You'll need to stop the server, replace the file, and then start the server. Hope it works.

Using this flow you should be able to FTP (localhost 2121), drop an assessment tar.gz.gpg file, and see assessment
records be logged.

#### Debugging
Simplest approach, add lots of logging statements in the code, and then tail `logs/nifi-app.log`. 
The log levels are set in `conf/logback.xml`.
I suggest setting the standard processors to at least INFO (they default to WARN) and, of course, set ours to INFO:
```xml
  <logger name="org.apache.nifi.processors" level="INFO"/>
  <logger name="rdw.nifi.processors" level="INFO"/>
```

You can also enable remote debugging by uncommenting a line in `conf/bootstrap.conf`:
```bash
java.arg.debug=-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005
```
In IntelliJ create a "Remote" configuration (with defaults) and connect.


#### Useful Links
Getting started: https://nifi.apache.org/docs/nifi-docs/html/getting-started.html
 * https://nifi.apache.org/docs/nifi-docs/components/
 * https://nifi.apache.org/docs/nifi-docs/components/org.apache.nifi.processors.standard.ValidateCsv/
 * https://nifi.apache.org/docs/nifi-docs/components/org.apache.nifi.processors.standard.EncryptContent/
 * https://nifi.apache.org/docs/nifi-docs/components/org.apache.nifi.processors.standard.IdentifyMimeType/
 * https://nifi.apache.org/docs/nifi-docs/components/org.apache.nifi.processors.standard.UnpackContent/

Source repo: https://github.com/apache/nifi 
https://github.com/apache/nifi/tree/master/nifi-nar-bundles/nifi-standard-bundle/nifi-standard-processors/src/main/java/org/apache/nifi/processors/standard
 
 
 ### Testing With Data
 
 The data generation project can be used to create assessment csv+json pairs.
 There is a submit.sh script that archives, encrypts, signs, then uploads them.
 
 On my mbp it was taking 7-12 sec to parse a CSV file.
 I didn't capture all the throughput numbers but ~75 files / 5 min = 4 sec per file.
 I need the asmts/sec number.
 