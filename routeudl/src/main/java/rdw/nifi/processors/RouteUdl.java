package rdw.nifi.processors;

import com.google.common.net.MediaType;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.lang3.mutable.MutableObject;
import org.apache.nifi.annotation.behavior.EventDriven;
import org.apache.nifi.annotation.behavior.InputRequirement;
import org.apache.nifi.annotation.behavior.SideEffectFree;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.stream.io.StreamUtils;
import rdw.cloud.stream.app.model.ContentMetaData;
import rdw.cloud.stream.app.model.JsonUtils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.apache.nifi.flowfile.attributes.CoreAttributes.FILENAME;
import static org.apache.nifi.flowfile.attributes.CoreAttributes.MIME_TYPE;

@EventDriven
@SideEffectFree
@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
@Tags({"rdw", "udl"})
@CapabilityDescription("Routes CSV content of UDL file (archive with CSV+JSON) based on metadata in JSON content")
public class RouteUdl extends AbstractProcessor {

    public static final Relationship REL_FAILURE = new Relationship.Builder().name("failure")
            .description("If a file cannot be processed, the original file will be routed to this destination and nothing will be routed elsewhere").build();
    public static final Relationship REL_ORIGINAL = new Relationship.Builder().name("original")
            .description("The original input file will be routed to this destination on success").build();
    public static final Relationship REL_ASSESSMENT = new Relationship.Builder().name("assessment")
            .description("An assessment CSV file will be routed to this destination").build();
    public static final Relationship REL_STUDENTREG = new Relationship.Builder().name("studentreg")
            .description("A student registration CSV will be routed to this destination").build();

    private Set<Relationship> relationships;

    @Override
    protected void init(final ProcessorInitializationContext context) {
        super.init(context);

        final Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_FAILURE);
        relationships.add(REL_ORIGINAL);
        relationships.add(REL_ASSESSMENT);
        relationships.add(REL_STUDENTREG);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return relationships;
    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        final FlowFile flowFile = session.get();
        if (flowFile == null) {
            return;
        }

        final MutableObject<ContentMetaData> meta = new MutableObject<>();
        final MutableObject<Exception> failure = new MutableObject<>();
        final MutableObject<FlowFile> csv= new MutableObject<>();
        session.read(flowFile, is -> {
            try (final ArchiveInputStream ais = new ArchiveStreamFactory().createArchiveInputStream(new BufferedInputStream(is))) {
                ArchiveEntry entry;
                while ((entry = ais.getNextEntry()) != null) {
                    if (entry.isDirectory()) continue;

                    if (entry.getName().toLowerCase().endsWith(".csv")) {
                        FlowFile csvFlowFile = session.create(flowFile);
                        csvFlowFile = session.write(csvFlowFile, out -> StreamUtils.copy(ais, out));
                        csvFlowFile = session.putAttribute(csvFlowFile, FILENAME.key(), entry.getName());
                        csvFlowFile = session.putAttribute(csvFlowFile, MIME_TYPE.key(), MediaType.CSV_UTF_8.toString());
                        csv.setValue(csvFlowFile);

                    } else if (entry.getName().toLowerCase().endsWith(".json")) {
                        meta.setValue(JsonUtils.contentMetaDataFromJson(ais));

                    } else {
                        getLogger().warn("Ignoring unexpected file {} in archive {}", new Object[]{entry.getName(), flowFile});

                    }
                }
            } catch (final IOException | ArchiveException e) {
                failure.setValue(e);
            }
        });

        if (failure.getValue() != null) {
            getLogger().warn("Failed to process {} due to {}, routing to failure", new Object[]{flowFile, failure.getValue()});
            session.transfer(flowFile, REL_FAILURE);
            return;
        }

        final ContentMetaData metaData = meta.getValue();
        if (metaData == null) {
            getLogger().warn("Missing metadata in {}, routing to failure", new Object[]{flowFile});
            session.transfer(flowFile, REL_FAILURE);
            return;
        }

        FlowFile csvFlowFile = csv.getValue();
        if (csvFlowFile == null) {
            getLogger().warn("No CSV file in {}, routing to failure", new Object[]{flowFile});
            session.transfer(flowFile, REL_FAILURE);
            return;
        }

        // TODO - put attributes based on metadata

        switch(metaData.getContent()) {
            case "assessment":
                session.transfer(csvFlowFile, REL_ASSESSMENT);
                session.transfer(flowFile, REL_ORIGINAL);
                break;
            case "StudentRegistration":
                session.transfer(csvFlowFile, REL_STUDENTREG);
                session.transfer(flowFile, REL_ORIGINAL);
                break;
            default:
                getLogger().warn("Unexpected '{}' content in {}, routing to failure", new Object[]{metaData.getContent(), flowFile});
                session.transfer(flowFile, REL_FAILURE);
                break;
        }

        session.commit();
    }
}
