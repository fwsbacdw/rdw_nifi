### TODO
There are a number of things to look into before the PoC can be graduated to a production solution. In no particular order ...

#### StudentRegistration
Implement the import of student registration CSV. And serialized POJO.

#### Unit Tests
Set up unit or integration tests. The Apache NiFi documentation touts some good test harness helpers for that.

#### SDLC
Apache NiFi does not have good support for the software deployment/promotion part of the development life cycle.
Figure something out.

#### Profiling
The Udl step (which parses the CSV) is taking 7-12 seconds per file. Figure out if that is all just parsing and 
marshaling or if there is some performance impact of creating thousands of flow files for the resulting objects.

#### POST XML
Demonstrate a ListenHttps to inject XML directly into flow. Should be easy enough but how to integrate with security? 