package rdw.nifi.processors;

import com.google.common.net.MediaType;
import org.apache.commons.lang3.mutable.MutableObject;
import org.apache.nifi.annotation.behavior.EventDriven;
import org.apache.nifi.annotation.behavior.InputRequirement;
import org.apache.nifi.annotation.behavior.SideEffectFree;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException;
import org.beanio.BeanReader;
import org.beanio.StreamFactory;
import rdw.cloud.stream.app.model.TDSReport;
import rdw.cloud.stream.app.model.XmlUtils;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static org.apache.nifi.flowfile.attributes.CoreAttributes.FILENAME;
import static org.apache.nifi.flowfile.attributes.CoreAttributes.MIME_TYPE;

@EventDriven
@SideEffectFree
@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
@Tags({"rdw", "udl", "csv", "assessment", "tdsreport"})
@CapabilityDescription("Processes an assessment CSV emitting TDSReports")
public class AssessmentCsvProcessor extends AbstractProcessor {

    public static final Relationship REL_FAILURE = new Relationship.Builder().name("failure")
            .description("If a file cannot be processed, the original file will be routed to this destination and nothing will be routed elsewhere").build();
    public static final Relationship REL_ORIGINAL = new Relationship.Builder().name("original")
            .description("The original input file will be routed to this destination on success").build();
    public static final Relationship REL_SUCCESS = new Relationship.Builder().name("success")
            .description("TDSReports generated from CSV rows will be routed to this destination").build();

    private Set<Relationship> relationships;

    private StreamFactory factory;

    @Override
    protected void init(final ProcessorInitializationContext context) {
        super.init(context);

        final Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_FAILURE);
        relationships.add(REL_ORIGINAL);
        relationships.add(REL_SUCCESS);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return relationships;
    }

    @OnScheduled
    public void createReaderFactory(final ProcessContext context) {
        // create the factory just once
        factory = StreamFactory.newInstance();
        factory.loadResource("mapping.xml");
    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        final FlowFile flowFile = session.get();
        if (flowFile == null) {
            return;
        }

        final List<TDSReport> reports = new ArrayList<>();
        final MutableObject<Exception> failure = new MutableObject<>();
        session.read(flowFile, is -> {
            final BeanReader reader = factory.createReader("assessment", new InputStreamReader(is));
            reader.setErrorHandler(ex -> {
                getLogger().warn(ex.toString());
                for (int i = 0, cnt = ex.getRecordCount(); i < cnt; i++) {
                    // TODO - do something to preserve record text, ex.getRecordContext(i).getRecordText()
                    // TODO - reject entire file or just bad records?
                }
            });
            // skip the header row
            reader.skip(1);

            TDSReport report;
            while ((report = (TDSReport) reader.read()) != null) {
                reports.add(report);
            }
        });

        if (failure.getValue() != null) {
            getLogger().warn("Failed to process {} due to {}, routing to failure", new Object[]{flowFile, failure.getValue()});
            session.transfer(flowFile, REL_FAILURE);
            return;
        }

        // this follows a pattern seen in SplitJson and SplitXml
        final String fragmentIdentifier = UUID.randomUUID().toString();
        final String filename = flowFile.getAttribute(FILENAME.key());
        final String fragmentCount = Integer.toString(reports.size());
        int index = 0;
        for (final TDSReport report : reports) {
            FlowFile reportFile = session.create(flowFile);
            reportFile = session.write(reportFile, out -> {
                XmlUtils.tdsReportToXml(report, out);
            });
            reportFile = session.putAttribute(reportFile, "fragment.identifier", fragmentIdentifier);
            reportFile = session.putAttribute(reportFile, "fragment.count", fragmentCount);
            reportFile = session.putAttribute(reportFile, "fragment.index", Integer.toString(index++));
            reportFile = session.putAttribute(reportFile, "segment.original.filename", filename);
            reportFile = session.putAttribute(reportFile, MIME_TYPE.key(), MediaType.APPLICATION_XML_UTF_8.toString());
            reportFile = session.putAttribute(reportFile, "rdw.content", "assessment");
            session.transfer(reportFile, REL_SUCCESS);
        }

        getLogger().info("Successfully created {} TDSReports from assessment CSV file {}", new Object[]{reports.size(), flowFile});

        session.transfer(flowFile, REL_ORIGINAL);
        session.commit();
    }
}
