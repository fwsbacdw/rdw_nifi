### Clustering Apache NiFi

It wasn't a complete slam-dunk to create a cluster that actually distributes the work ...

Basic clustering docs: https://nifi.apache.org/docs/nifi-docs/html/administration-guide.html#clustering
Be wary of the HortonWorks documentation on this topic; it appears there is some additional functionality not apparent
in the open source / community version of the software.

#### Setting up on AWS

1. Create instances. From EC2 Management Console, Launch Instance
    1. Amazon Linux (fyi, user is `ec2-user`)
    1. t2.xlarge
    1. 3 instances (you'll want to rename instances after creating them, e.g. nifi-1, nifi-2, nifi-3)
    1. 30 GB storage (not enough really)
    1. security instance with open ports: 22, 2110, 2111, 2121, 2181, 2888, 3888, 8080
    1. dev ssh key 
2. On each node install updates, java 8
```bash
sudo yum -y update
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u111-b14/jdk-8u111-linux-x64.rpm"
sudo yum -y install jdk-8u111-linux-x64.rpm
rm jdk-8u111-linux-x64.rpm 
```
Modify `~/.bash_profile` to set up java
```bash
export JAVA_HOME=/usr/java/jdk1.8.0_111
export JRE_HOME=$JAVA_HOME/jre
PATH=$PATH:$HOME/.local/bin:$HOME/bin:$JAVA_HOME/bin:$JRE_HOME/bin
```
3. On each node add entries to `/etc/hosts` for each node. Use the private IP for current node, public IP for others. For example,
```bash
172.31.1.37   nifi-1
54.218.111.103  nifi-2
54.202.205.34   nifi-3
```
This is a good time to set up /etc/hosts and ~/.ssh/config on dev/build machine.
4. On each node, install Apache NiFi and RDW processors (https://nifi.apache.org/docs/nifi-docs/html/administration-guide.html#how-to-install-and-start-nifi)
```bash
wget -c http://mirrors.koehn.com/apache/nifi/1.1.1/nifi-1.1.1-bin.tar.gz
tar xzvf nifi-1.1.1-bin.tar.gz
rm nifi-1.1.1-bin.tar.gz
mkdir nifi-1.1.1/data_in/
mkdir nifi-1.1.1/error_out/
```
From dev/build machine (assuming ssh set up nicely):
```bash
for s in 1 2 3; do scp nar/target/nifi-rdw-nar-1.0-SNAPSHOT.nar nifi-$s:nifi-1.1.1/lib/; done
for s in 1 2 3; do scp flow.xml.gz nifi-$s:nifi-1.1.1/conf/; done
for s in 1 2 3; do scp secring.gpg nifi-$s:nifi-1.1.1/conf/; done
```
5. On each node configure NiFi for clustering with embedded zookeeper (https://nifi.apache.org/docs/nifi-docs/html/administration-guide.html#clustering).
Replace `nifi-X` with current node hostname, e.g. `nifi-1`.
    1. In `./conf/nifi.properties`
    ```bash
    nifi.cluster.is.node=true
    nifi.cluster.node.address=nifi-X
    nifi.state.management.embedded.zookeeper.start=true
    nifi.zookeeper.connect.string=nifi-1:2181,nifi-2:2181,nifi-3:2181
    nifi.cluster.node.protocol.port=2111
    nifi.cluster.flow.election.max.candidates=3
    nifi.remote.input.host=nifi-X
    nifi.remote.input.socket.port=2110
    nifi.web.http.host=nifi-X
    ```
    2. In `./conf/zookeeper.properties`
    ```bash
    server.1=nifi-1:2888:3888
    server.2=nifi-2:2888:3888
    server.3=nifi-3:2888:3888
    ```
    3. Create zookeeper id file (replace X with 1, 2 or 3 based on current node)
    ```bash
    mkdir -p ./state/zookeeper
    echo X > ./state/zookeeper/myid
    ```
    4. (Optional) Adjust log levels in `./conf/logback.xml`
    ```xml
      <logger name="org.apache.nifi" level="WARN"/>
      <logger name="rdw.nifi.processors" level="INFO"/>
    ```
6. Run the nifi instances. From the dev/build machine:
```bash
for s in 1 2 3; do ssh nifi-$s sudo nifi-1.1.1/bin/nifi.sh start; done
```

At this point, the cluster should be running. You can test by hitting `nifi-1:8080/nifi`.

#### Adjusting Flow 

The standalone flow won't distribute work yet; each node will only process files dropped locally. 
To fix that the flow needs to be modified. Since it is the CSV processing that is the bottleneck, 
that work should be distributed.
1. From the hamburger menu, select Cluster and note which node is the `COORDINATOR`. For this example it is nifi-3.
2. Delete the link between `ProcessGpgAsUdl` and `ProcessAssessmentCsv`.
3. Create a new Input Port, call it whatever (rpg-assessment-csv) and connect it to `ProcessAssessmentCsv`.
4. Create a new Remote Process Group (RPG).
    1. URL = http://nifi-3:8080/nifi
    1. Transport Protocol = HTTP
5. Connect `ProcessGpgAsUdl` output to RPG.

#### Restarting

NOTE: if the cluster is stopped / restarted, you'll need to fix things up:
 1. Update the /etc/hosts files on all systems
 2. If the COORDINATOR changes you'll need to recreate the RPG (can't just change URL)

### Testing With Data

From the data-generator project, create the gpg files and drop them on one (or more) of the nodes.
```bash
scp ASMT_ID*.gpg nifi-1:nifi-1.1.1/data_in/
```

On the server instances, each CSV is processed in about 5.x sec, and there are 3 instances so the effective time is
1.8 sec, which is 5-6x faster than standalone on my dev box.
Overall CSV throughput: 160 files / 5 min ~ 0.5 files/sec.
Overall TDSReport throughput: 75000 / 5 min ~ 250/sec.
