package rdw.nifi.processors;


import com.google.common.net.MediaType;
import org.apache.commons.lang3.mutable.MutableObject;
import org.apache.nifi.annotation.behavior.EventDriven;
import org.apache.nifi.annotation.behavior.InputRequirement;
import org.apache.nifi.annotation.behavior.SideEffectFree;
import org.apache.nifi.annotation.behavior.SupportsBatching;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.flowfile.attributes.CoreAttributes;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.util.StandardValidators;
import org.apache.nifi.util.StopWatch;
import org.bouncycastle.openpgp.PGPException;
import org.c02e.jpgpj.Decryptor;
import org.c02e.jpgpj.FileMetadata;
import org.c02e.jpgpj.Key;
import org.c02e.jpgpj.Ring;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@EventDriven
@SideEffectFree
@SupportsBatching
@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
@Tags({"rdw", "decryption", "PGP", "GPG"})
@CapabilityDescription("Decrypts a FlowFile using PGP")
public class DecryptContent extends AbstractProcessor {

    public static final PropertyDescriptor KEYRING = new PropertyDescriptor.Builder()
            .name("keyring-file")
            .displayName("Keyring File")
            .description("Comma-delimited list of keyring or key files which contain the private key of the recipient for encryption and/or the public keys of signers for validation")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();
    public static final PropertyDescriptor KEYRING_PASSPHRASE = new PropertyDescriptor.Builder()
            .name("keyring-passphrase")
            .displayName("Keyring Passphrase")
            .description("This is the keyring passphrase used to unlock the private key of the recipient")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .sensitive(true)
            .build();

    public static final Relationship REL_SUCCESS = new Relationship.Builder().name("success")
            .description("Any FlowFile that is successfully decrypted and validated will be routed to success").build();
    public static final Relationship REL_FAILURE = new Relationship.Builder().name("failure")
            .description("Any FlowFile that cannot be decrypted and validated will be routed to failure").build();

    private List<PropertyDescriptor> properties;
    private Set<Relationship> relationships;

    private Decryptor decryptor;

    @Override
    protected void init(final ProcessorInitializationContext context) {
        super.init(context);

        final List<PropertyDescriptor> properties = new ArrayList<>();
        properties.add(KEYRING);
        properties.add(KEYRING_PASSPHRASE);
        this.properties = Collections.unmodifiableList(properties);

        final Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_SUCCESS);
        relationships.add(REL_FAILURE);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    protected List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return properties;
    }

    @Override
    public Set<Relationship> getRelationships() {
        return relationships;
    }

    @OnScheduled
    public void createDecryptor(final ProcessContext context) {
        final Ring ring = new Ring();
        for (final String token : context.getProperty(KEYRING).getValue().split(",")) {
            try {
                ring.load(new File(token));
            } catch (final IOException | PGPException e) {
                getLogger().error("Error loading keyring file {} due to {}", new Object[]{token, e});
            }
        }
        final String passphrase = context.getProperty(KEYRING_PASSPHRASE).getValue();
        for (final Key key : ring.getDecryptionKeys()) {
            key.setPassphrase(passphrase);
        }
        decryptor = new Decryptor(ring);
    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        if (flowFile == null) {
            return;
        }

        try {
            final StopWatch stopWatch = new StopWatch(true);
            final MutableObject<FileMetadata> meta = new MutableObject<>();  // TODO - assuming bad to modify flowFile inside write()
            flowFile = session.write(flowFile, (in, out) -> {
                try {
                    meta.setValue(decryptor.decrypt(in, out));
                } catch (PGPException e) {
                    throw new ProcessException(e);
                }
            });
            flowFile = session.putAttribute(flowFile, CoreAttributes.MIME_TYPE.key(), MediaType.APPLICATION_BINARY.toString());
            flowFile = session.putAttribute(flowFile, CoreAttributes.FILENAME.key(), meta.getValue().getName());
            getLogger().info("successfully decrypted {}", new Object[]{flowFile});
            session.getProvenanceReporter().modifyContent(flowFile, stopWatch.getElapsed(TimeUnit.MILLISECONDS));
            session.transfer(flowFile, REL_SUCCESS);
        } catch (final ProcessException e) {
            getLogger().error("Cannot decrypt {} - ", new Object[]{flowFile, e});
            session.transfer(flowFile, REL_FAILURE);
        }
        session.commit();
    }
}
